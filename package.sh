#!/bin/bash

set -ex

if [ "$#" -lt 4 ] ; then
	echo "Usage: $0 <board> <u-boot-build-path> <fipdir> <fip-output-dir>"
	exit 1
fi

BOARD=$1
UBOOT=$2
FIPDIR=$3
FIPOUT=$4

case $BOARD in
	"odroid-c2")
	FIPDIR="$FIPDIR/odroid-c2"
    SOC="odroid-c2"
    ;;
	"odroid-c4")
	FIPDIR="$FIPDIR/odroid-c4"
    SOC="sm1"
    ;;
	"odroid-hc4")
	FIPDIR="$FIPDIR/odroid-hc4"
    SOC="sm1"
    ;;
	"odroid-go-ultra")
	FIPDIR="$FIPDIR/odroid-go-ultra"
    SOC="g12b"
    ;;
	"odroid-n2")
	FIPDIR="$FIPDIR/odroid-n2"
    SOC="g12b"
    ;;
	"odroid-n2l")
	FIPDIR="$FIPDIR/odroid-n2l"
    SOC="g12b"
    ;;
	"p212")
	FIPDIR="$FIPDIR/p212"
    SOC="gxl"
    ;;
	"s400")
	FIPDIR="$FIPDIR/s400"
    SOC="axg"
    ;;
	"p201")
	FIPDIR="$FIPDIR/p201"
    SOC="gxbb"
    ;;
	"p200")
	FIPDIR="$FIPDIR/p200"
    SOC="gxbb"
    ;;
	"u200")
	FIPDIR="$FIPDIR/u200"
    SOC="g12a"
    ;;
	"libretech-cc")
	FIPDIR="$FIPDIR/lepotato"
    SOC="gxl"
    ;;
	"libretech-cc_v2")
	FIPDIR="$FIPDIR/lepotato"
    SOC="gxl"
    ;;
	"libretech-ac")
	FIPDIR="$FIPDIR/lafrite"
    SOC="gxl"
    ;;
	"libretech-s905d-pc")
	FIPDIR="$FIPDIR/tartiflette-s905d"
    SOC="gxl"
    ;;
	"libretech-s912-pc")
	FIPDIR="$FIPDIR/tartiflette-s912"
    SOC="gxm"
    ;;
	"khadas-vim")
	FIPDIR="$FIPDIR/khadas-vim"
    SOC="gxl"
    ;;
	"khadas-vim2")
	FIPDIR="$FIPDIR/khadas-vim2"
    SOC="gxm"
    ;;
	"khadas-vim3"*)
	FIPDIR="$FIPDIR/khadas-vim3"
    SOC="g12b"
    ;;
	"khadas-vim3l"*)
	FIPDIR="$FIPDIR/khadas-vim3l"
    SOC="sm1"
    ;;
	"nanopi-k2")
	FIPDIR="$FIPDIR/nanopi-k2"
    SOC="gxbb"
    ;;
	"sei510")
	FIPDIR="$FIPDIR/sei510"
    SOC="g12a"
    ;;
	"sei610")
	FIPDIR="$FIPDIR/sei610"
    SOC="sm1"
    ;;
	"wetek-core2")
	FIPDIR="$FIPDIR/wetek-core2"
    SOC="gxl"
    ;;
	"wetek-hub")
	FIPDIR="$FIPDIR/wetek-hub"
    SOC="gxbb"
    ;;
	"wetek-play2")
	FIPDIR="$FIPDIR/wetek-play2"
    SOC="gxbb"
    ;;
	"beelink-gtking")
	FIPDIR="$FIPDIR/beelink-s922x"
    SOC="g12b"
    ;;
	"beelink-gtkingpro")
	FIPDIR="$FIPDIR/beelink-s922x"
    SOC="g12b"
    ;;
	"bananapi-cm4-cm4io")
	FIPDIR="$FIPDIR/bananapi-cm4io"
    SOC="g12b"
    ;;
	"bananapi-m2-pro")
	FIPDIR="$FIPDIR/bananapi-m2-pro"
    SOC="sm1"
    ;;
	"bananapi-m2s")
	FIPDIR="$FIPDIR/bananapi-m2s"
    SOC="g12b"
    ;;
	"bananapi-m5")
	FIPDIR="$FIPDIR/bananapi-m5"
    SOC="sm1"
    ;;
	"radxa-zero")
	FIPDIR="$FIPDIR/radxa-zero"
    SOC="g12a"
    ;;
	"radxa-zero2")
	FIPDIR="$FIPDIR/radxa-zero2"
    SOC="g12b"
    ;;
    *)
	echo "FIXME: No FIP for $BOARD"
	exit 0
esac

TMPDIR=$(mktemp -d)
mkdir -p $FIPOUT

make -C $FIPDIR TMP=${TMPDIR} O=${FIPOUT} BL33=$UBOOT/u-boot.bin

ls ${FIPOUT}/

exit 0
